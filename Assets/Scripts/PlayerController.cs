﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundry
{
	public float xMin;
	public float xMax;
	public float zMin;
	public float zMax;
	public float yMin;
	public float yMax;
}

public class PlayerController : MonoBehaviour 
{
	public float boltOffsetX;
	public float boltOffsetY;
	public float speed;
	public Boundry boundry;
	public float tilt;

	public GameObject shot;
	public GameObject topCamera;
	public Transform shotSpawn;
	public float fireRate;

	private float nextFire;

	// Use this for initialization
	void Start() 
	{
	
	}
	
	// Update is called once per frame
	void Update() 
	{
		if((Input.GetButton("Fire1") || Input.GetKeyDown("space")) && Time.time > nextFire)
		{
			nextFire = Time.time + fireRate;

			Vector3 shotOnePosition = shotSpawn.position;
			Vector3 shotTwoPosition = shotSpawn.position;
			Vector3 shotThreePosition = shotSpawn.position;
			Vector3 shotFourPosition = shotSpawn.position;

			shotOnePosition.x = shotOnePosition.x + boltOffsetX;
			shotOnePosition.y = shotOnePosition.y + boltOffsetY;

			shotTwoPosition.x = shotTwoPosition.x - boltOffsetX;
			shotTwoPosition.y = shotTwoPosition.y - boltOffsetY;

			shotThreePosition.x = shotThreePosition.x + boltOffsetX;
			shotThreePosition.y = shotThreePosition.y - boltOffsetY;

			shotFourPosition.x = shotFourPosition.x - boltOffsetX;
			shotFourPosition.y = shotFourPosition.y + boltOffsetY;

			Instantiate(shot, shotOnePosition, shotSpawn.rotation);

			Instantiate(shot, shotTwoPosition, shotSpawn.rotation);
			Instantiate(shot, shotThreePosition, shotSpawn.rotation);
			Instantiate(shot, shotFourPosition, shotSpawn.rotation);

			GetComponent<AudioSource>().Play();
		}
	}

	void FixedUpdate()
	{
		// float moveHorizontal = Input.GetAxis("Horizontal");
		// float moveForward = Input.GetAxis("Vertical");

		float moveHorizontal = Input.acceleration.x;
		float moveForward = Input.acceleration.y;

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveForward);
		GetComponent<Rigidbody>().velocity = movement * speed;

		GetComponent<Rigidbody>().position = new Vector3
			(
				Mathf.Clamp (GetComponent<Rigidbody>().position.x, boundry.xMin, boundry.xMax), 
				0.0f, 
				Mathf.Clamp (GetComponent<Rigidbody>().position.z, boundry.zMin, boundry.zMax)
				);

		GetComponent<Rigidbody>().rotation = 
			Quaternion.Euler (0.0f,0.0f,GetComponent<Rigidbody>().velocity.x * -tilt);
	}
}
