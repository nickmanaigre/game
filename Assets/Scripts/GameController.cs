﻿using UnityEngine;
using System;
using System.Collections;

public class GameController : MonoBehaviour 
{
	public GameObject mainCamera;
	public GameObject rPGCamera;
	public GameObject topCamera;

	public GameObject[] hazards;
	public GameObject[] enemies;

	public int maxHazardCount;
	public int maxEnemyCount;
	public Vector3 spawnValues;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	public GUIText scoreText;
	public GUIText restartText;
	public GUIText gameOverText;
	public GUIText waveNumberText;

	public GameObject pressRestart;

	private bool gameOver;
	private bool restart;
	private int score;

	void Start()
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		mainCamera.GetComponent<Camera>().enabled = false;
		rPGCamera.GetComponent<Camera>().enabled = true;

		pressRestart.active = false;

		gameOver = false;
		restart = false;

		restartText.text = "";
		gameOverText.text = "";
		waveNumberText.text = "0";

		score = 0;
		UpdateScore();

		StartCoroutine(SpawnWaves());
	}

	void Update()
	{
		if(restart || pressRestart)
		{
			if(Input.GetKeyDown(KeyCode.R))
			{
				Application.LoadLevel(Application.loadedLevel);
			}
		}
		if(Input.GetKeyDown(KeyCode.C))
		{
			ChangeCamera();
		}
	}

	public void GameOver()
	{
		gameOverText.text = "GameOver";
		gameOver = true;

		mainCamera.GetComponent<Camera>().enabled = true;
		rPGCamera.GetComponent<Camera>().enabled = false;
	}

	private String GetWaveText(int wave)
	{
		String waveNumber = wave.ToString();
		return String.Format("{0} {1}", "Wave: ", waveNumber);
	}

	private int GetHazardCount(int wave)
	{
		int hazardCount;
		hazardCount = ((wave/5) + 1) * 2 + (wave%5);

		return hazardCount;
	}

	IEnumerator SpawnWaves()
	{
		int hazardCount;
		int enemyCount; 

		yield return new WaitForSeconds(startWait);

		for(int wave = 1; wave < 100 && !restart; wave++)
		{
			waveNumberText.text = GetWaveText(wave);

			hazardCount = GetHazardCount(wave);
			Debug.Log("wave: " + wave + " hazardCount: " + hazardCount);
			enemyCount = GetHazardCount(wave);


			StartCoroutine(SpawnObstacles(hazards, hazardCount));
			StartCoroutine(SpawnObstacles(enemies, enemyCount));

			yield return new WaitForSeconds(waveWait);

			if(gameOver)
			{
				restartText.text = "Press 'R' for Restart";
				restart = true;
				pressRestart.active = true;
			}
		}
	}

	IEnumerator SpawnObstacles(GameObject[] obstacles, int amount)
	{
		for(int i = 0; i < amount; i++)
		{
			Vector3 spawnPosition = new Vector3(UnityEngine.Random.Range(-spawnValues.x, spawnValues.x), 
				spawnValues.y , spawnValues.z);

			Quaternion spawnRotation = Quaternion.identity;
			Instantiate (obstacles[0], spawnPosition, spawnRotation);

			yield return new WaitForSeconds(spawnWait);
		}
	}

	public void AddScore(int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore();
	}

	void UpdateScore()
	{
		scoreText.text = "Score: " + score;
	}

	void ChangeCamera()
	{
		if(rPGCamera && rPGCamera.GetComponent<Camera>().enabled && topCamera)
		{
			topCamera.GetComponent<Camera>().enabled = true;
			rPGCamera.GetComponent<Camera>().enabled = false;
		}
		else if (topCamera && rPGCamera)
		{
			topCamera.GetComponent<Camera>().enabled = false;
			rPGCamera.GetComponent<Camera>().enabled = true;
		}
	}
}
