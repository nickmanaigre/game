﻿using UnityEngine;
using System.Collections;

public class ChangeCameraPosition : MonoBehaviour 
{
	public Transform target;
	public float distanceY;
	public float distanceZ;
	public float distanceX;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update(){

		if(target)
		{
	 		transform.position = new Vector3(
		 		target.transform.position.x + distanceX, 
		 		target.transform.position.y + distanceY, 
		 		target.transform.position.z + distanceZ);
		}


	}
}
