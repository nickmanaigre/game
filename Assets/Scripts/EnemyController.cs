﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour 
{
	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate;

	public float boltOffsetX;
	public float boltOffsetY;

	private float nextFire;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Time.time > nextFire)
		{
			nextFire = Time.time + fireRate;

			Vector3 shotOnePosition = shotSpawn.position;
			Vector3 shotTwoPosition = shotSpawn.position;
			

			shotOnePosition.x = shotOnePosition.x + boltOffsetX;
			shotOnePosition.y = shotOnePosition.y + boltOffsetY;

			shotTwoPosition.x = shotTwoPosition.x - boltOffsetX;
			shotTwoPosition.y = shotTwoPosition.y + boltOffsetY;

			Instantiate(shot, shotOnePosition, shotSpawn.rotation);
			Instantiate(shot, shotTwoPosition, shotSpawn.rotation);

			// GetComponent<AudioSource>().Play();
		}
	}
}
